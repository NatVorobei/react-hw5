import { useFormik } from "formik";
import { validationSchema } from "./validationSchema";
import './form.scss';
import { removeCartItemsFromLocalStorage } from "../../redux/action/products";
import { useDispatch } from "react-redux";


export default function Form(){
    const dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            age: 0,
            shippingAddress: '',
            phoneNumber: ''
        },
        validationSchema: validationSchema,
        // validateOnChange: false,
        // validateOnBlur: false,
        onSubmit: (values) => {
            const cartItems = JSON.parse(localStorage.getItem('cartItems'));
            console.log('Purchased Items:', cartItems);
            console.log('Form Data:', values);
            dispatch(removeCartItemsFromLocalStorage());
            alert('Your purchase was successful!')
        }
    })

    return (
        <div className="checkoutForm">
            <h2>Checkout</h2>
        <form onSubmit={formik.handleSubmit}>
            <div className="inputs">
            <div className="formgroup">
            <label htmlFor="firstName">
            {formik.errors.firstName ? (
                <span style={{color: 'red'}}>{formik.errors.firstName}</span>
                ) : (
                'First Name:'
            )}
            </label>
                <input 
                    type="text"
                    name="firstName" 
                    id="firstName"
                    value={formik.values.firstName}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                />
            </div>
            <div className="formgroup">
                <label htmlFor="lastName">
                {formik.errors.lastName ? (
                    <span style={{color: 'red'}}>{formik.errors.lastName}</span>
                    ) : (
                    'Last Name:'
                )}
                </label>
                <input 
                    type="text"
                    name="lastName" 
                    id="lastName"
                    value={formik.values.lastName}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                />
            </div>
            <div className="formgroup">
                <label htmlFor="age">
                {formik.errors.age ? (
                    <span style={{color: 'red'}}>{formik.errors.age}</span>
                    ) : (
                    'Age:'
                )}
                </label>
                <input 
                    type="number"
                    name="age" 
                    id="age"
                    value={formik.values.age}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                />
            </div>
            <div className="formgroup">
                <label htmlFor="shippingAddress">
                {formik.errors.shippingAddress ? (
                    <span style={{color: 'red'}}>{formik.errors.shippingAddress}</span>
                    ) : (
                    'Shipping Address:'
                )}
                </label>
                <input 
                    type="text" 
                    name="shippingAddress" 
                    id="shippingAddress" 
                    value={formik.values.shippingAddress} 
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                />
            </div>
            <div className="formgroup">
                <label htmlFor="phoneNumber">
                {formik.errors.phoneNumber ? (
                    <span style={{color: 'red'}}>{formik.errors.phoneNumber}</span>
                    ) : (
                    'Phone Number:'
                )}
                </label>
                <input 
                    type="text"
                    name="phoneNumber" 
                    id="phoneNumber"
                    value={formik.values.phoneNumber}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                />
            </div>
            </div>
            <div>
                <button type="submit">Checkout</button>
            </div>
        </form>
        </div>
    )
}