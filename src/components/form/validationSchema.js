import * as Yup from 'yup';

export const validationSchema = Yup.object({
    firstName: Yup.string()
        .matches(/^[А-ЯІЇЄҐа-яіїєґ'ҐA-Za-z\s]+$/u, 'Only letters')
        .min(2, 'Min 2 letters required')
        .max(20, 'Only 20 letters allowed')
        .required('This field is required!'),
    lastName: Yup.string()
        .matches(/^[А-ЯІЇЄҐа-яіїєґ'ҐA-Za-z\s]+$/u, 'Only letters')
        .min(2, 'Min 2 letters required')
        .max(20, 'Only 20 letters allowed')
        .required('This field is required!'),
    age: Yup.number()
        .min(16, 'Min age is 16')
        .max(100, 'Max age is 100')
        .required('this field is required!'),
    shippingAddress: Yup.string()
        .min(5, 'Min 5 letters required')
        .max(20, 'Only 20 letters allowed')
        .required('This field is required!'),
    phoneNumber: Yup.string()
        .matches(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/, 'Incorrect number')
        .min(9, 'Min 9 symbols required')
        .max(10, 'Only 10 symbols allowed')
        .required('This field is required!'),
});