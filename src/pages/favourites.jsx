import { useSelector } from "react-redux";
import ProductList from "../components/cards/product-list";

export function Favourites(props){
    const favourites = useSelector(state => state.productsReducer.favourites);
    return (
        <>
            <h1 className="page_title" style={{textAlign: 'center', color: '#007eb9'}}>Favourites</h1>
            {favourites.length > 0 ? (
                <ProductList
                products={props.products}
                cartItems={props.cartItems}
                favourites={props.favourites}
                // favourites={favourites}
                onAddProductToCart={props.onAddProductToCart}
                onRemoveProductFromCart={props.onRemoveProductFromCart}
                onAddProductToFavs={props.onAddProductToFavs}
                onRemoveProductFromFavs={props.onRemoveProductFromFavs}  
                />
            ) : (
                <p style={{ textAlign: 'center', color: '#818181', fontSize: '18px'}}>
                    No favourites added.
                </p>
            )}
        </>
    )
}