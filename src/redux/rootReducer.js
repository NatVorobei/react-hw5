// Core
import { combineReducers } from "redux";
import { productsReducer } from "./reducers/products";
import { modalsReducer } from "./reducers/modals";
// Reducers

export const rootReducer = combineReducers({
    productsReducer,
    modalsReducer
});
